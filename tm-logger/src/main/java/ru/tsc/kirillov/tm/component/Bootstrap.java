package ru.tsc.kirillov.tm.component;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.IReceiverService;
import ru.tsc.kirillov.tm.listener.LoggerListener;
import ru.tsc.kirillov.tm.service.ReceiverService;

public final class Bootstrap {
    
    public void run(@Nullable final String[] args) {
        @NotNull final ActiveMQConnectionFactory factory =
                new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

}
