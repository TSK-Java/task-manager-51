package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.kirillov.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.kirillov.tm.dto.model.UserDTO;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.user.UserNameAlreadyExistsException;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;
import ru.tsc.kirillov.tm.util.HashUtil;

import java.util.UUID;

public final class UserServiceTest extends AbstractTest {

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private ITaskServiceDTO taskService;

    @NotNull final String userLogin = UUID.randomUUID().toString();

    @NotNull final String userPassword = UUID.randomUUID().toString();

    @NotNull final String userEmail = UUID.randomUUID().toString();

    @Before
    @Override
    public void initialization() {
        super.initialization();
        projectService = new ProjectServiceDTO(CONNECTION_SERVICE);
        taskService = new TaskServiceDTO(CONNECTION_SERVICE);
    }

    @After
    @Override
    public void finalization() {
        super.finalization();
        taskService.clear();
        projectService.clear();
    }

    @Test
    public void create() {
        @Nullable final String email = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "", ""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, null, email));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, "", ""));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, null, email));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create(userLogin, userPassword, ""));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create(userLogin, userPassword, email));
        Assert.assertEquals(0, userService.count());
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertNotNull(user);
        Assert.assertEquals(1, userService.count());
        @Nullable UserDTO userFind = userService.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
        Assert.assertEquals(user.getEmail(), userFind.getEmail());
        Assert.assertEquals(user.getLogin(), userLogin);
        Assert.assertEquals(user.getEmail(), userEmail);
        Assert.assertThrows(
                UserNameAlreadyExistsException.class, () -> userService.create(userLogin, userPassword, userEmail)
        );
        @NotNull UserDTO userRandom = userService.create("random", "random", "random@domain.com");
        Assert.assertThrows(
                EmailAlreadyExistsException.class,
                () -> userService.create(userRandom.getId(), userPassword, userEmail)
        );
    }

    @Test
    public void createRole() {
        @Nullable final Role role = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "", "", role));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, null, null, role));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, "", "", role));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, null, null, role));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create(userLogin, userPassword, userEmail, role));
        Assert.assertEquals(0, userService.count());
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail, Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(1, userService.count());
        @Nullable UserDTO userFind = userService.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
        Assert.assertEquals(user.getRole(), userFind.getRole());
        Assert.assertEquals(user.getLogin(), userLogin);
        Assert.assertEquals(user.getRole(), Role.ADMIN);
        Assert.assertThrows(
                UserNameAlreadyExistsException.class, () -> userService.create(userLogin, userPassword, userEmail, Role.ADMIN)
        );
    }

    @Test
    public void isLoginExists() {
        Assert.assertEquals(0, userService.count());
        Assert.assertFalse(userService.isLoginExists(""));
        Assert.assertFalse(userService.isLoginExists(null));
        Assert.assertFalse(userService.isLoginExists(userLogin));
        userService.create(userLogin, userPassword, userEmail);
        Assert.assertTrue(userService.isLoginExists(userLogin));
    }

    @Test
    public void isEmailExists() {
        Assert.assertEquals(0, userService.count());
        Assert.assertFalse(userService.isLoginExists(""));
        Assert.assertFalse(userService.isLoginExists(null));
        Assert.assertFalse(userService.isLoginExists(userLogin));
        userService.create(userLogin, userPassword, userEmail);
        Assert.assertTrue(userService.isEmailExists(userEmail));
        Assert.assertFalse(userService.isEmailExists(UUID.randomUUID().toString()));
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(0, userService.count());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertNull(userService.findByLogin(userLogin));
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail);
        @Nullable UserDTO userFind = userService.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
        Assert.assertEquals(user.getEmail(), userFind.getEmail());
        Assert.assertNull(userService.findByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void findByEmail() {
        Assert.assertEquals(0, userService.count());
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(""));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));
        Assert.assertNull(userService.findByEmail(userEmail));
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail);
        @Nullable UserDTO userFind = userService.findByEmail(user.getEmail());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
        Assert.assertEquals(user.getEmail(), userFind.getEmail());
        Assert.assertNull(userService.findByEmail(UUID.randomUUID().toString()));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, userService.count());
        Assert.assertNull(userService.remove(null));
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertEquals(1, userService.count());
        @Nullable UserDTO userRemove = userService.remove(user);
        Assert.assertNotNull(userRemove);
        Assert.assertEquals(user.getLogin(), userRemove.getLogin());
        Assert.assertEquals(0, userService.count());
    }

    @Test
    public void removeByLogin() {
        Assert.assertEquals(0, userService.count());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertEquals(1, userService.count());
        @Nullable UserDTO userRemove = userService.removeByLogin(user.getLogin());
        Assert.assertNotNull(userRemove);
        Assert.assertEquals(user.getLogin(), userRemove.getLogin());
        Assert.assertEquals(0, userService.count());
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin(user.getLogin()));
    }

    @Test
    public void setPassword() {
        Assert.assertEquals(0, userService.count());
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail);
        @NotNull String userId = user.getId();
        @NotNull String newPassword = UUID.randomUUID().toString();
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword("", ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword(null, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(userId, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(userId, ""));
        Assert.assertNull(userService.setPassword(UUID.randomUUID().toString(), newPassword));
        @Nullable UserDTO userPass = userService.setPassword(userId, newPassword);
        Assert.assertNotNull(userPass);
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, newPassword), userPass.getPasswordHash());
    }

    @Test
    public void updateUser() {
        Assert.assertEquals(0, userService.count());
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail);
        @NotNull String userId = user.getId();
        @NotNull String newFirstName = UUID.randomUUID().toString();
        @NotNull String newLastName = UUID.randomUUID().toString();
        @NotNull String newMiddleName = UUID.randomUUID().toString();
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.updateUser("", "", "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.updateUser(null, null, null, null)
        );
        Assert.assertNull(
                userService.updateUser(UUID.randomUUID().toString(), "", "", "")
        );
        @Nullable UserDTO userPass = userService.updateUser(userId, newFirstName, newLastName, newMiddleName);
        Assert.assertNotNull(userPass);
        Assert.assertEquals(newFirstName, userPass.getFirstName());
        Assert.assertEquals(newLastName, userPass.getLastName());
        Assert.assertEquals(newMiddleName, userPass.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertEquals(0, userService.count());
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        Assert.assertThrows(
                UserNotFoundException.class, () -> userService.lockUserByLogin(UUID.randomUUID().toString())
        );
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(user.getLogin());
        @Nullable UserDTO userFind = userService.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertTrue(userFind.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertEquals(0, userService.count());
        @NotNull UserDTO user = userService.create(userLogin, userPassword, userEmail);
        user.setLocked(true);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(""));
        Assert.assertThrows(
                UserNotFoundException.class, () -> userService.unlockUserByLogin(UUID.randomUUID().toString())
        );
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(user.getLogin());
        @Nullable UserDTO userFind = userService.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertFalse(userFind.getLocked());
    }

}
