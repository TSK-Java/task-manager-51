package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import ru.tsc.kirillov.tm.model.User;

public abstract class AbstractUserTest extends AbstractTest {

    @NotNull
    protected User userTest;

    @NotNull
    protected User userAdmin;

    @NotNull
    protected String userId;

    @Before
    @Override
    public void initialization() {
        super.initialization();
        userTest = userService.create("test", "test", "test@domain.com");
        userId = userTest.getId();
        userAdmin = userService.create("admin", "admin", "admin@domain.com");
    }

}
