package ru.tsc.kirillov.tm.exception.field;

public final class NameEmptyException extends AbstractFieldException {

    public NameEmptyException() {
        super("Ошибка! Имя не задано.");
    }

}
