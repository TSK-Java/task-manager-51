package ru.tsc.kirillov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public class ProjectGetByIdResponse extends AbstractProjectResponse {

    public ProjectGetByIdResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}
