package ru.tsc.kirillov.tm.exception.system.value;

import ru.tsc.kirillov.tm.util.DateUtil;

import java.util.Date;

public final class ValueNotValidDate extends AbstractValueException {

    public ValueNotValidDate() {
        super("Ошибка! Значение не является датой.");
    }

    public ValueNotValidDate(final String value, final String formatDate) {
        super(
                String.format("Ошибка! Значение `%s` не является датой. Ожидаемый формат даты: `%s` (пример: %s)",
                        value == null ? "null" : value,
                        formatDate == null ? "" : formatDate,
                        DateUtil.toString(new Date())
                )
        );
    }

}
